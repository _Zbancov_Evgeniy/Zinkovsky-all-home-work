// 1-calculation
var x = 6;
var y = 14;
var z = 4;
x+= y - x++*z;
document.write('1) x+= y - x++*z = ',+ x, '<br>');
// 1) x++*z = 6*4 = 24, 2) y-24=14-24=-10,  3) x=x-10=6-10=-4.

// 2-calculation
var x = 6;
var y = 14;
var z = 4;
z =--x-y*5;
document.write('2) z =--x-y*5 = ',+ z, '<br>');
// 1)--x=6-1=5, 2) y*5=14*5=70, 3) z=5-70=-65.

// 3-calculation
var x = 6;
var y = 14;
var z = 4;
y/= x + 5%z;
document.write('3) y/= x + 5%z = ',+ y, '<br>');
// 1) 5%z=5%4=1, 2) x+1=6+1=7, 3) y=y/7=14/7=2.

// 4-calculation
var x = 6;
var y = 14;
var z = 4;
// z - x++ +y*5; - якщо цей рядок розкоментувати, то буде 67.
res = z - x++ + y * 5;
document.write('4) z - x++ + y * 5 = ',+ res, '<br>');
// 1) y*5=14*5=70, 2) z-x=4-6=-2, 3) -2+70=68.

// 5-calculation
var x = 6;
var y = 14;
var z = 4;
x = y - x++*z;
document.write('5) x = y - x++*z = ',+ x, '<br>');
// 1) x++*z=6*4=24, 2) y-24=14-24=-10, 3) x=-10.
