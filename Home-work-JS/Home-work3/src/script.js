var styles = ['Джаз', 'Блюз']
document.write('Так виглягає масив спочатку - ' + styles.join(', ') + '<br><hr>');
styles.push('Рок-н-рол');
document.write('Так виглягає масив після добавляння - ' + styles.join(', ') + '<br><hr>');
let stlength = styles.length;
let half = stlength%2;
switch (half) { 
  case 1: {
    styles.splice(stlength/2, 1, 'Класика');
    break;
  }
  case 0: {
    styles.splice(stlength/2, 0, 'Класика');
  }
    break;
  default: { 
    document.write('Error');
  }
}
document.write('Так виглягає масив після заміни середнього елемента - ' + styles.join(', ') + '<br><hr>');
styles.shift();
document.write('Так виглягає масив після видалення першого елемента - ' + styles.join(', ') + '<br><hr>');
styles.unshift('Реп', 'Реггі');
document.write('Так виглягає масив після добавлення у початок елементів Реп і Реггі - ' + styles.join(', ') + '<br><hr>');




